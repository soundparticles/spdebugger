/*
  ==============================================================================

    FileListBox.cpp
    Created: 30 Apr 2018 4:24:05pm
    Author:  Ashley

  ==============================================================================
*/

#include "FileListBox.h"

FileListBox::FileListBox() : ListBox("ValuesList", 0)
{
	setModel(this);
	setMultipleSelectionEnabled(false);

	setColour(ColourIds::textColourId, Colours::white);
	setColour(ColourIds::backgroundColourId, Colour(40, 51, 56));
	setColour(ColourIds::outlineColourId, Colours::transparentWhite);

	getVerticalScrollBar().setColour(ScrollBar::ColourIds::thumbColourId, Colour(117, 183, 158));
}

int FileListBox::getNumRows()
{
	return _files.size();
}

void FileListBox::listBoxItemClicked(int row, const MouseEvent& event)
{
	if (_listener)
		_listener->listItemSelected(row);
}

void FileListBox::paintListBoxItem(int rowNumber, Graphics& g, int width, int height, bool rowIsSelected)
{
	if (rowIsSelected)
		g.fillAll(Colour(70, 81, 86));

	int margins = 5;
	int wn = (width - 2 * margins)*0.6f;
	int ws = (width - 2 * margins)*0.4f;
	g.setColour(findColour(textColourId));
	g.setFont(height * 0.7f);
	g.drawText(_files[rowNumber].name, margins, 0, wn, height, Justification::centredLeft, true);

	g.setColour(Colour(117, 183, 158));
	g.setFont(height * 0.5f);
	g.drawText(getSizeString(_files[rowNumber].size), margins + wn, 0, ws, height, Justification::centredRight, true);
}

void FileListBox::paint(Graphics& g)
{
	g.fillAll(findColour(backgroundColourId));
}

void FileListBox::selectItem(int id)
{
	selectRow(id);
}

void FileListBox::add(String file, int64 size)
{
	FileInfo fi;
	fi.name = file;
	fi.size = size;
	_files.add(fi);
	updateContent();
}

void FileListBox::clearList()
{
	_files.clear();
	updateContent();
}

String FileListBox::getSizeString(int64 size)
{
	String s;
	float sz;

	if (size > 1073741824)
	{
		s = "GB";
		sz = (float)size / 1073741824;
	}
	else if (size > 1048576)
	{
		s = "MB";
		sz = (float)size / 1048576;
	}
	else if (size > 1024)
	{
		s = "KB";
		sz = (float)size / 1024;
	}
	else
	{
		s = "B";
	}

	return String(sz, 3) + " " + s;
}

void FileListBox::setListener(Listener * l)
{
	_listener = l;
}
