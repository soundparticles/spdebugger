/*
  ==============================================================================

    SimpleListBox.cpp
    Created: 30 Apr 2018 1:44:33pm
    Author:  Ashley

  ==============================================================================
*/

#include "ValuesListBox.h"

ValuesListBox::ValuesListBox() : ListBox("ValuesList", 0)
{
	setModel(this);
	setMultipleSelectionEnabled(false);

	setColour(ColourIds::textColourId, Colours::white);
	setColour(ColourIds::backgroundColourId, Colour(40, 51, 56));
	setColour(ColourIds::outlineColourId, Colours::transparentWhite);

	getVerticalScrollBar().setColour(ScrollBar::ColourIds::thumbColourId, Colour(117, 183, 158));
}
int ValuesListBox::getNumRows()
{
	return _values.size();
}

void ValuesListBox::paintListBoxItem(int rowNumber, Graphics& g, int width, int height, bool rowIsSelected)
{
	if (rowIsSelected)
		g.fillAll(Colour(70, 81, 86));

	int margins = 5;
	int numW = (int)((width - margins * 2) * 0.4f);
	int valW = (int)((width - margins * 2) * 0.6f);

	g.setColour(Colour(117, 183, 158));
	g.setFont(height * 0.5f);
	g.drawText(String(rowNumber), margins, 0, numW, height, Justification::centredLeft, true);

	g.setColour(findColour(textColourId));
	g.setFont(height * 0.7f);
	g.drawText(String(_values[rowNumber], 6), margins + numW, 0, valW, height, Justification::centredRight, true);
}

void ValuesListBox::paint(Graphics& g)
{
	g.fillAll(findColour(backgroundColourId));
}

void ValuesListBox::setValues(const float* buf, int size)
{
	_values.clear();

	for (int i = 0; i < size; ++i)
		_values.push_back(buf[i]);

	updateContent();
}

void ValuesListBox::clearList()
{
	_values.clear();
	updateContent();
}
