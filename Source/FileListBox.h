/*
  ==============================================================================

    FileListBox.h
    Created: 30 Apr 2018 4:24:05pm
    Author:  Ashley

  ==============================================================================
*/

#pragma once
#include "../JuceLibraryCode/JuceHeader.h"

class FileListBox : public ListBox, public ListBoxModel
{
public:
	class Listener
	{
	public:
		virtual void listItemSelected(int id) = 0;
	};

	FileListBox();

	int getNumRows();

	void listBoxItemClicked(int row, const MouseEvent& event);

	void paintListBoxItem(int rowNumber, Graphics& g, int width, int height, bool rowIsSelected);
	void paint(Graphics& g);

	void selectItem(int id);
	void add(String file, int64 size);
	void clearList();
	String getSizeString(int64 size);

	void setListener(Listener* l);

private:
	struct FileInfo
	{
		String name;
		int size;
	};
	Array<FileInfo> _files;
	Listener* _listener = nullptr;
};