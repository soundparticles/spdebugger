#include "MainComponent.h"

MainComponent::MainComponent()
{
	setSize(600, 600);
    setAudioChannels (2, 2);

	_zoomPosScroll.setLookAndFeel(&_lfScrollBar);
	_zoomScroll.setLookAndFeel(&_lfScrollBar);

	_selectFolderLabel.setText("", NotificationType::dontSendNotification);
	_selectFolderLabel.setColour(Label::ColourIds::outlineColourId, Colour(91,102,107));
	_selectFolderLabel.setColour(Label::ColourIds::backgroundColourId, Colour(40,51,56));
	_zoomPosScroll.setColour(ScrollBar::ColourIds::thumbColourId, Colour(117, 183, 158));
	_zoomScroll.setColour(ScrollBar::ColourIds::thumbColourId, Colour(117, 183, 158));
	_zoomPosScroll.setColour(ScrollBar::ColourIds::trackColourId, Colour(40, 51, 56));
	_zoomScroll.setColour(ScrollBar::ColourIds::trackColourId, Colour(40, 51, 56));

	_selectFolderButton.setButtonText("Select Folder");
	_clearButton.setButtonText("Delete All");
	_hideFilesButton.setButtonText("Hide Old Files");
	_playButton.setButtonText("Play");
	_zoomInButton.setButtonText("+");
	_zoomOutButton.setButtonText("-");

	_zoomPosScroll.setRangeLimits(Range<double>(0.0, 1.0));
	_zoomScroll.setRangeLimits(Range<double>(0.0, 1.0));
	_zoomPosScroll.setCurrentRange(Range<double>(0.0, 1.0));
	_zoomScroll.setCurrentRange(Range<double>(1.0 - 0.05, 1.0));

	_selectFolderButton.addListener(this);
	_playButton.addListener(this);
	_hideFilesButton.addListener(this);
	_clearButton.addListener(this);
	_zoomInButton.addListener(this);
	_zoomOutButton.addListener(this);
	_zoomScroll.addListener(this);
	_zoomPosScroll.addListener(this);
	_filesList.setListener(this);

	_playButton.setEnabled(false);

	_zoomPosScroll.setAutoHide(false);
	_zoomScroll.setAutoHide(false);

	addAndMakeVisible(_valuesList);
	addAndMakeVisible(_selectFolderLabel);
	addAndMakeVisible(_selectFolderButton);
	addAndMakeVisible(_playButton);
	addAndMakeVisible(_hideFilesButton);
	addAndMakeVisible(_clearButton);
	addAndMakeVisible(_filesList);
	addAndMakeVisible(_zoomPosScroll);
	addAndMakeVisible(_zoomScroll);
	addAndMakeVisible(_zoomInButton);
	addAndMakeVisible(_zoomOutButton);

	loadSettings();
}

MainComponent::~MainComponent()
{
	_zoomPosScroll.setLookAndFeel(nullptr);
	_zoomScroll.setLookAndFeel(nullptr);
    shutdownAudio();
}

void MainComponent::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
}

void MainComponent::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
{
	bufferToFill.clearActiveBufferRegion();

	if (_playing)
	{
		const float* buf = _buffer->getReadPointer(0);

		int rest = _buffer->getNumSamples() - (bufferToFill.numSamples + _bufPos);
		int samplesToRead;
		if (rest <= bufferToFill.numSamples)
		{
			samplesToRead = rest;
			_playing = false;
			_playButton.setButtonText("Play");
		}
		else
			samplesToRead = bufferToFill.numSamples;

		for (int c = 0; c < bufferToFill.buffer->getNumChannels(); ++c)
		{
			float* outBuf = bufferToFill.buffer->getWritePointer(c);

			for (int i = 0; i < samplesToRead; ++i)
				outBuf[i] = buf[_bufPos + i];

		}

		_bufPos = (_bufPos + bufferToFill.numSamples) % _buffer->getNumSamples();
	}
}

void MainComponent::releaseResources()
{
	saveSettings();
	if (_buffer) delete(_buffer);
}

void MainComponent::paint (Graphics& g)
{
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

	int drawHeight = (_graphRect.getHeight() - 20) / 2; // Margins
	if (drawHeight < 0) drawHeight = _graphRect.getHeight();

	int middleLineYPos = _graphRect.getY() + _graphRect.getHeight() / 2;

	g.setColour(Colours::slategrey);
	g.drawRect(_graphRect, 1);

	g.setColour(Colours::dimgrey);
	g.drawLine(_graphRect.getX(), middleLineYPos, _graphRect.getX() + _graphRect.getWidth(), middleLineYPos, 1.0f);

	if (!_buffer) return;
	const float* buffer = _buffer->getReadPointer(0);

	int totalSamples = _buffer->getNumSamples();
	float viewSamples = jmax((int)(_buffer->getNumSamples() * _zoom), 1); // Number of samples to show in the graph
	float samplesPerBlock = (float)viewSamples / _graphRect.getWidth();

	float maxGlobalValue = std::abs(getMaxSampleValue(buffer, 0, totalSamples - 1));
	if (maxGlobalValue == 0.0f) maxGlobalValue = 1.0f;

	g.setColour(Colour(36, 204, 88));

	Point<float> p1;
	Point<float> p2;
	Range<double> r = getRangeFromPos(_zoomPos, viewSamples / totalSamples); // Viewport relative range
	int startSample = totalSamples * r.getStart();
	float blocksPerSample = _graphRect.getWidth() / viewSamples;

	if (samplesPerBlock < 1.0f) // Draw by connecting values
	{
		for (int s = 0; s < (int)viewSamples; ++s)
		{
			int ps = s == 0 ? 0 : (blocksPerSample / 2.0f) + blocksPerSample * s;
			int pe = s == viewSamples ? _graphRect.getWidth() : (blocksPerSample / 2.0f) + blocksPerSample * (s + 1);
			float valueS = buffer[startSample + s];
			float valueE = buffer[startSample + s + 1];

			p1.setXY(jmax(_graphRect.getX() + 1, _graphRect.getX() + ps), middleLineYPos - (valueS / maxGlobalValue) * drawHeight);
			p2.setXY(jmin(_graphRect.getRight() - 1, _graphRect.getX() + pe), middleLineYPos - (valueE / maxGlobalValue) * drawHeight);

			Line<float> line(p1, p2);
			g.drawLine(line, 1.0f);

			int squareSize = 4;

			if (samplesPerBlock < 0.25f) // Draw by connecting values
				g.fillRect((float)p1.x - squareSize / 2, (float)p1.y - squareSize / 2, (float)squareSize, (float)squareSize);
		}
	}
	else // Draw vertical line connecting max and min
	{
		for (int p = 0; p < _graphRect.getWidth(); ++p)
		{
			int start = startSample + p * samplesPerBlock;
			int end = startSample + (p + 1) * samplesPerBlock - 1;

			float maxValue, minValue;
			getMinMaxValues(buffer, start, end, minValue, maxValue);
			if (isnan(maxValue) || isnan(minValue)) continue;

			if (maxValue == minValue) // If samplesPerBlock is below 2, only 1 sample is analised, therefor max and min are the same
				minValue = 0.0f;

			p1.setXY(_graphRect.getX() + p, middleLineYPos - (maxValue / maxGlobalValue) * drawHeight);
			p2.setXY(_graphRect.getX() + p, middleLineYPos - (minValue / maxGlobalValue) * drawHeight);

			Line<float> line(p1, p2);
			g.drawLine(line, 1.0f);
		}
	}
}

double MainComponent::getPosFromRange(Range<double> range)
{
	if (range.getLength() == 1.0)
		return 0.0;
	else
		return range.getStart() / (1.0 - range.getLength());
}

Range<double> MainComponent::getRangeFromPos(double pos, double size)
{
	double start = pos * (1.0 - size);
	return Range<double>(start, start + size);
}

float MainComponent::getMaxSampleValue(const float* samples, int startSample, int endSample)
{
	float max = 0.0f;
	int maxSample = -1;

	for (int s = startSample; s < endSample; ++s)
	{
		if (std::abs(samples[s]) > max)
		{
			max = std::abs(samples[s]);
			maxSample = s;
		}
	}

	return samples[maxSample];
}

void MainComponent::getMinMaxValues(const float * samples, int startSample, int endSample, float & _min, float & _max)
{
	float max = samples[startSample];
	float min = samples[startSample];

	for (int s = startSample; s < endSample; ++s)
	{
		if (samples[s] > max)
		{
			max = samples[s];
		}
		if (samples[s] < min)
		{
			min = samples[s];
		}
	}

	_min = min;
	_max = max;
}

void MainComponent::resized()
{
	int originalWidth = 1500;
	int originalHeight = 1200;

	Rectangle<int> main(originalWidth, originalHeight);

	int margins = 10;
	main.removeFromTop(margins);
	main.removeFromLeft(margins);

	Rectangle<int> rect = main.removeFromLeft(400);

	Rectangle<int> selectFolderRect = rect.removeFromTop(30);
	_selectFolderButton.setBounds(selectFolderRect.removeFromRight(100));
	selectFolderRect.removeFromRight(10);
	_selectFolderLabel.setBounds(selectFolderRect);

	rect.removeFromTop(10);
	Rectangle<int> graphRect = rect.removeFromTop(200);
	Rectangle<int> zoomRect = graphRect.removeFromRight(20);
	zoomRect.removeFromBottom(20);
	graphRect.removeFromRight(10);
	_zoomPosScroll.setBounds(graphRect.removeFromBottom(10));
	graphRect.removeFromBottom(10);
	_graphRect = graphRect;
	rect.removeFromTop(10);

	_zoomInButton.setBounds(zoomRect.removeFromTop(20));
	zoomRect.removeFromTop(10);
	_zoomOutButton.setBounds(zoomRect.removeFromBottom(20));
	zoomRect.removeFromBottom(10);
	zoomRect.removeFromRight(5);
	zoomRect.removeFromLeft(5);
	_zoomScroll.setBounds(zoomRect);

	Rectangle<int> playRect = rect.removeFromTop(30);
	_playButton.setBounds(playRect.removeFromLeft(50));
	_clearButton.setBounds(playRect.removeFromRight(80));
	playRect.removeFromRight(10);
	_hideFilesButton.setBounds(playRect.removeFromRight(100));

	int height = _playButton.getBounds().getBottom() - margins;

	Rectangle<int> listRect = main.removeFromTop(height);
	listRect.removeFromLeft(10);
	_filesList.setBounds(listRect.removeFromLeft(150));
	listRect.removeFromLeft(10);
	_valuesList.setBounds(listRect.removeFromLeft(170));

	int width = _valuesList.getBounds().getRight() - margins;

	setSize(width + margins * 2, height + margins * 2);
}

void MainComponent::loadSettings()
{
	_options.applicationName = "AudioDebugger";
	_options.filenameSuffix = "settings";
	_options.folderName = "AudioDebuggerData";
	_options.osxLibrarySubFolder = "Application Support";
	_options.commonToAllUsers = false;
	_options.storageFormat = PropertiesFile::storeAsBinary;
	_settings.setStorageParameters(_options);

	_propertiesFile = _settings.getUserSettings();

    String path = _propertiesFile->getValue("datafolder");
    
    if (path != "")
    {
        _folder = File(path);
        
        if (!_folder.exists())
            _folder = File();
    }
    else
        _folder = File();
    
	updateFolder();
	startTimer(1000);
}

void MainComponent::saveSettings()
{
	if (_folder.exists())
		_propertiesFile->setValue("datafolder", _folder.getFullPathName());
	else
		_propertiesFile->removeValue("datafolder");

	_propertiesFile->saveIfNeeded();
}

void MainComponent::updateFolder()
{
    if (!_folder.exists())
        _folder = File();
    
	updateFolderLabelText(_folder.getFullPathName());
	getFiles();

	selectBuffer(_selectedFile);
}

void MainComponent::updateFolderLabelText(String path)
{
	if (path.length() > 20)
		path = "..." + path.substring(path.lastIndexOfChar(File::getSeparatorChar()));

	_selectFolderLabel.setText(path, NotificationType::dontSendNotification);
}

void MainComponent::clearAllFiles()
{
	FileSearchPath path(_folder.getFullPathName());
    Array<File> files = path.findChildFiles(File::TypesOfFileToFind::findFiles, false, "*.csv; *.bin");

	for (File f : files)
		f.deleteFile();

	updateFolder();
}

void MainComponent::addZoom(float zoom)
{
	_zoom += zoom;

	if (_zoom > 1.0) _zoom = 1.0;
	if (_zoom < 0.0) _zoom = 0.0;

	_zoomScroll.setCurrentRange(getRangeFromPos(pow(_zoom, 0.25f), _zoomScroll.getCurrentRangeSize()));

	repaint();
}

void MainComponent::buttonClicked(Button * button)
{
	if (button == &_selectFolderButton)
	{
		FileChooser chooser("Select data folder");

		if (chooser.browseForDirectory())
		{
			_folder = chooser.getResult();
			updateFolder();
		}
	}
	else if (button == &_playButton)
	{
		if (_playing)
		{
			_playing = false;
			_playButton.setButtonText("Play");
		}
		else
		{
			_bufPos = 0;
			_playing = true;
			_playButton.setButtonText("Stop");
		}
	}
	else if (button == &_hideFilesButton)
		updateFolder();
	else if (button == &_clearButton)
		clearAllFiles();
	else if (button == &_zoomInButton)
		addZoom(-_zoom / 10);
	else if (button == &_zoomOutButton)
		addZoom(_zoom / 10);
}

void MainComponent::getFiles()
{
	_files.clear();
	_filesList.clearList();

	if (!_folder.exists())
		return;

	FileSearchPath path(_folder.getFullPathName());
	Array<File> files = path.findChildFiles(File::TypesOfFileToFind::findFiles, false, "*.csv; *.bin");

	bool hideFiles = _hideFilesButton.getToggleState();

	for (File f : files)
		if (!hideFiles || Time::getCurrentTime() - f.getLastModificationTime() < RelativeTime(60 * 60 * 24)) // File was created less than 24 hours ago
			_files.add(f);

	FileSorter sorter;
	_files.sort(sorter);

	for (File f : _files)
		_filesList.add(getCommentFromFileName(&f), f.getSize());
}

String MainComponent::getCommentFromFileName(File* file)
{
	return file->getFileNameWithoutExtension().substring(6);
}

void MainComponent::updateValuesList()
{
	if (_buffer)
		_valuesList.setValues(_buffer->getReadPointer(0), _buffer->getNumSamples());
	else
		_valuesList.clearList();
}

void MainComponent::listItemSelected(int id)
{
	selectBuffer(id);
	_selectedFile = id;
}

void MainComponent::selectBuffer(int id)
{
	if (id == -1) return;

	if (_oldFile != _files[id])
	{
		if (_files.size() <= id || id < 0)
		{
			if (_buffer) delete(_buffer);
			_buffer = nullptr;
			_zoomPosScroll.setEnabled(false);
			_zoomScroll.setEnabled(false);
			updateValuesList();
			return;
		}

		_oldFile = _files[id];
		FileInputStream* stream = _files[id].createInputStream();
        std::vector<float> values;
        
        if(_files[id].getFileExtension()=="csv")
        {
            while (!stream->isExhausted())
                values.push_back(stream->readNextLine().getFloatValue());
        }
        else
        {
            while (!stream->isExhausted())
                values.push_back(stream->readDouble());
        }
        
		delete(stream);

		if (_buffer) delete(_buffer);
		_buffer = new AudioSampleBuffer(1, values.size());

		float* buf = _buffer->getWritePointer(0);
		for (int s = 0; s < values.size(); ++s)
			buf[s] = values[s];

		//_zoomStep = 1.0f / pow(_buffer->getNumSamples(), 0.7f);

		_playButton.setEnabled(true);
		_zoomPosScroll.setEnabled(true);
		_zoomScroll.setEnabled(true);
	}
	
	_filesList.selectItem(id);

	updateValuesList();
	repaint();
}

void MainComponent::timerCallback()
{
	updateFolder();
}

void MainComponent::scrollBarMoved(ScrollBar* scrollBarThatHasMoved, double newRangeStart)
{
	if (scrollBarThatHasMoved == &_zoomScroll)
	{
		double pos = getPosFromRange(_zoomScroll.getCurrentRange());
		_zoom = pos*pos*pos*pos;
		_zoomPosScroll.setCurrentRange(getRangeFromPos(_zoomPos, _zoom));
		repaint();
	}
	else if (scrollBarThatHasMoved == &_zoomPosScroll)
	{
		_zoomPos = getPosFromRange(_zoomPosScroll.getCurrentRange());
		repaint();
	}
}
