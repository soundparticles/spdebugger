#pragma once

#define MAX_FILES 100
#define MIN_ZOOM 0.00000001

#include "../JuceLibraryCode/JuceHeader.h"
#include "ValuesListBox.h"
#include "FileListBox.h"

class MainComponent : public AudioAppComponent, public Button::Listener, public FileListBox::Listener, 
	public Timer, public ScrollBar::Listener
{
public:
	MainComponent();
	~MainComponent();

	void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;
	void getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill) override;
	void releaseResources() override;

	void paint(Graphics& g) override;
	void resized() override;

	void selectBuffer(int id);
	void timerCallback() override; 
	void scrollBarMoved(ScrollBar* scrollBarThatHasMoved, double newRangeStart);

private:
	void loadSettings();
	void saveSettings();
	void updateFolder();
	void updateFolderLabelText(String path);

	void clearAllFiles();
	void addZoom(float zoom);

	void buttonClicked(Button* button) override;
	void getFiles();
	String getCommentFromFileName(File* file);
	void updateValuesList();

	void listItemSelected(int id) override;

	double getPosFromRange(Range<double> range);
	Range<double> getRangeFromPos(double pos, double size);
	float getMaxSampleValue(const float* samples, int startSample, int endSample);
	void getMinMaxValues(const float * samples, int startSample, int endSample, float & _min, float & _max);

	class FileSorter
	{
	public:
		static int compareElements(File file1, File file2)
		{
			return (int)(file2.getLastModificationTime().toMilliseconds() - file1.getLastModificationTime().toMilliseconds());
		}
	};

private:
	File _folder;
	Array<File> _files;
	File _oldFile;
	int _selectedFile = -1;

	ApplicationProperties _settings;
	PropertiesFile::Options _options;
	PropertiesFile* _propertiesFile = nullptr;

	AudioSampleBuffer* _buffer = nullptr;

	TextButton _selectFolderButton;
	TextButton _playButton;
	TextButton _clearButton;
	ToggleButton _hideFilesButton;
	Label _selectFolderLabel;
	ValuesListBox _valuesList;
	FileListBox _filesList;
	ScrollBar _zoomPosScroll{ false };
	ScrollBar _zoomScroll{ true };
	TextButton _zoomInButton;
	TextButton _zoomOutButton;

	class ScrollLookAndFeel : public LookAndFeel_V3
	{
	public:
		ScrollLookAndFeel() {};

		void drawScrollbar(Graphics& g, ScrollBar& scrollbar, int x, int y, int width, int height,
			bool isScrollbarVertical, int thumbStartPosition, int thumbSize, bool isMouseOver, bool isMouseDown) override
		{
			Path slotPath;

			const float slotIndent = jmin(width, height) > 15 ? 1.0f : 0.0f;
			const float slotIndentx2 = slotIndent * 2.0f;
			const float thumbIndent = slotIndent + 1.0f;
			const float thumbIndentx2 = thumbIndent * 2.0f;

			if (isScrollbarVertical)
				slotPath.addRoundedRectangle(x + slotIndent, y + slotIndent, width - slotIndentx2, height - slotIndentx2, (width - slotIndentx2) * 0.5f);
			else
				slotPath.addRoundedRectangle(x + slotIndent, y + slotIndent, width - slotIndentx2, height - slotIndentx2, (height - slotIndentx2) * 0.5f);

			g.setColour(scrollbar.findColour(ScrollBar::trackColourId));
			g.fillPath(slotPath);

			Path thumbPath;

			if (thumbSize > 0)
			{
				const float thumbIndent = (isScrollbarVertical ? width : height) * 0.25f;
				const float thumbIndentx2 = thumbIndent * 2.0f;

				if (isScrollbarVertical)
					thumbPath.addRoundedRectangle(x + thumbIndent, thumbStartPosition + thumbIndent,
						width - thumbIndentx2, thumbSize - thumbIndentx2, (width - thumbIndentx2) * 0.5f);
				else
					thumbPath.addRoundedRectangle(thumbStartPosition + thumbIndent, y + thumbIndent,
						thumbSize - thumbIndentx2, height - thumbIndentx2, (height - thumbIndentx2) * 0.5f);
			}

			Colour thumbCol(scrollbar.findColour(ScrollBar::thumbColourId, true));

			if (isMouseOver || isMouseDown)
				thumbCol = thumbCol.withMultipliedAlpha(2.0f);

			g.setColour(thumbCol);
			g.fillPath(thumbPath);

			g.setColour(thumbCol.contrasting((isMouseOver || isMouseDown) ? 0.2f : 0.1f));
			g.strokePath(thumbPath, PathStrokeType(1.0f));
		}

	} _lfScrollBar;

	Rectangle<int> _graphRect;
	double _zoom = 1.0;
	double _zoomPos = 0.0;

	bool _playing = false;
	int64 _bufPos = 0;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
