/*
  ==============================================================================

    ValuesListBox.h
    Created: 30 Apr 2018 1:44:33pm
    Author:  Ashley

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

class ValuesListBox : public ListBox, public ListBoxModel
{
public:
	ValuesListBox();

	int getNumRows();

	void paintListBoxItem(int rowNumber, Graphics& g, int width, int height, bool rowIsSelected);
	void paint(Graphics& g);

	void setValues(const float* buf, int size);
	void clearList();

private:
	std::vector<float> _values;
};